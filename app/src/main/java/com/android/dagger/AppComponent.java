package com.android.dagger;

import com.android.movies.MainActivity;
import com.android.services.IMDBService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainActivity mainActivity);

    void inject(IMDBService imdbService);
}
