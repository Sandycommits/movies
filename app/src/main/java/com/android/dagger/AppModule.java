package com.android.dagger;

import android.app.Application;

import androidx.room.Room;

import com.android.database.AppDatabase;
import com.android.movies.MyApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application application;

    public AppModule(MyApplication myApplication) {
        this.application = myApplication;
    }

    @Singleton
    @Provides
    AppDatabase getAppDatabase() {
        return Room.databaseBuilder(application,
                AppDatabase.class, "database-name").build();
    }
}
