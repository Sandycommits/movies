package com.android.services;

import android.app.Application;

import androidx.annotation.NonNull;

import com.android.database.AppDatabase;
import com.android.model.Movie;
import com.android.model.MovieDetails;
import com.android.model.MovieSearchResponse;
import com.android.movies.MyApplication;
import com.android.network.ApiClient;
import com.android.network.ApiInterface;
import com.android.utils.ProcessingState;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.android.network.ApiClient.API_KEY;

public class IMDBService {

    @Inject
    AppDatabase appDatabase;

    public IMDBService(Application application) {
        ((MyApplication) application).getAppComponent().inject(this);
    }

    public Single<ProcessingState> fetchMovies(String query, int page) {
        return Single.create(subscriber -> {
            ApiInterface apiInterface = ApiClient.getRetrofitInstance();
            Map<String, String> queryMap = new HashMap<>();
            queryMap.put("s", query);
            queryMap.put("page", String.valueOf(page));
            queryMap.put("apikey", API_KEY);
            Call<MovieSearchResponse> call = apiInterface.getMoviesList(queryMap);
            call.enqueue(new Callback<MovieSearchResponse>() {
                @Override
                public void onResponse(@NonNull Call<MovieSearchResponse> call, @NonNull Response<MovieSearchResponse> response) {
                    if (response.body() != null) {
                        ProcessingState.successState.extras(response.body());
                    }
                    subscriber.onSuccess(ProcessingState.successState);
                }

                @Override
                public void onFailure(@NonNull Call<MovieSearchResponse> call, @NonNull Throwable t) {
                    subscriber.onError(new Exception("Failed to fetch movies"));
                }
            });
        });
    }

    public Single<ProcessingState> fetchMovieDetails(String name) {
        return Single.create(subscriber -> {
            ApiInterface apiInterface = ApiClient.getRetrofitInstance();
            Map<String, String> queryMap = new HashMap<>();
            queryMap.put("t", name);
            queryMap.put("apikey", API_KEY);
            Call<MovieDetails> call = apiInterface.getMovieDetails(queryMap);
            call.enqueue(new Callback<MovieDetails>() {
                @Override
                public void onResponse(@NonNull Call<MovieDetails> call, @NonNull Response<MovieDetails> response) {
                    if (response.body() != null) {
                        ProcessingState.successState.extras(response.body());
                    }
                    subscriber.onSuccess(ProcessingState.successState);
                }

                @Override
                public void onFailure(@NonNull Call<MovieDetails> call, @NonNull Throwable t) {
                    subscriber.onError(new Exception("Failed to fetch movies"));
                }
            });
        });
    }

    public Single<ProcessingState> fetchLikedMovies() {
        return Single.create(subscriber -> {
            List<Movie> movies = appDatabase.movieDao().getAllLikedMovies();
            ProcessingState.successState.extras(movies);
            subscriber.onSuccess(ProcessingState.successState);
        });
    }

    public Single<ProcessingState> saveMovie(Movie movie) {
        return Single.create(subscriber -> {
            appDatabase.movieDao().insert(movie);
            subscriber.onSuccess(ProcessingState.successState);
        });
    }

    public Single<ProcessingState> removeMovie(Movie movie) {
        return Single.create(subscriber -> {
            appDatabase.movieDao().remove(movie);
            subscriber.onSuccess(ProcessingState.successState);
        });
    }
}
