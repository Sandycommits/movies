package com.android.network;

import com.android.model.MovieDetails;
import com.android.model.MovieSearchResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @GET("/?")
    Call<MovieSearchResponse> getMoviesList(@QueryMap Map<String, String> query);

    @GET("/?")
    Call<MovieDetails> getMovieDetails(@QueryMap Map<String, String> query);
}
