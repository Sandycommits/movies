package com.android.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class Movie {

    @ColumnInfo(name = "title")
    @SerializedName("Title")
    private String title;

    @ColumnInfo(name = "year")
    @SerializedName("Year")
    private String year;

    @ColumnInfo(name = "poster")
    @SerializedName("Poster")
    private String poster;

    @PrimaryKey
    @NonNull
    @SerializedName("imdbID")
    private String id;

    @ColumnInfo(name = "isLiked")
    private boolean isChecked;
}
