package com.android.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
public class MovieSearchResponse {

    @Setter
    @SerializedName("Search")
    private List<Movie> moviesList;

    @SerializedName("totalResults")
    private int totalResults;

    @SerializedName("Response")
    private boolean isResponse;
}
