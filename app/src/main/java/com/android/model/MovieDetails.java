package com.android.model;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

@Getter
public class MovieDetails {

    @SerializedName("Title")
    private String title;

    @SerializedName("Released")
    private String releaseDate;

    @SerializedName("Genre")
    private String genre;

    @SerializedName("Runtime")
    private String runtime;

    @SerializedName("Poster")
    private String poster;

    @SerializedName("imdbRating")
    private String rating;

    @SerializedName("Plot")
    private String overview;

}
