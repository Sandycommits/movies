package com.android.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.model.Movie;
import com.android.services.IMDBService;
import com.android.utils.ProcessingState;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MoviesViewModel extends AndroidViewModel {

    private IMDBService imdbService;

    public MoviesViewModel(@NonNull Application application) {
        super(application);
        imdbService = new IMDBService(application);
    }

    public MutableLiveData<ProcessingState> fetchMoviesList(String query, int page) {
        MutableLiveData<ProcessingState> moviesLiveData = new MutableLiveData<>();
        imdbService.fetchMovies(query, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess(moviesLiveData::postValue)
                .doOnError(throwable -> moviesLiveData.postValue(ProcessingState.errorState))
                .subscribe();
        return moviesLiveData;
    }

    public MutableLiveData<ProcessingState> fetchMovieDetails(String name) {
        MutableLiveData<ProcessingState> moviesLiveData = new MutableLiveData<>();
        imdbService.fetchMovieDetails(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess(moviesLiveData::postValue)
                .doOnError(throwable -> moviesLiveData.postValue(ProcessingState.errorState))
                .subscribe();
        return moviesLiveData;
    }

    public MutableLiveData<ProcessingState> fetchLikedMoviesList() {
        MutableLiveData<ProcessingState> moviesLiveData = new MutableLiveData<>();
        imdbService.fetchLikedMovies()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess(moviesLiveData::postValue)
                .doOnError(throwable -> moviesLiveData.postValue(ProcessingState.errorState))
                .subscribe();
        return moviesLiveData;
    }

    public MutableLiveData<ProcessingState> saveMovie(Movie movie) {
        MutableLiveData<ProcessingState> moviesLiveData = new MutableLiveData<>();
        imdbService.saveMovie(movie)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess(moviesLiveData::postValue)
                .doOnError(throwable -> moviesLiveData.postValue(ProcessingState.errorState))
                .subscribe();
        return moviesLiveData;
    }

    public MutableLiveData<ProcessingState> removeMovie(Movie movie) {
        MutableLiveData<ProcessingState> moviesLiveData = new MutableLiveData<>();
        imdbService.removeMovie(movie)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .doOnSuccess(moviesLiveData::postValue)
                .doOnError(throwable -> moviesLiveData.postValue(ProcessingState.errorState))
                .subscribe();
        return moviesLiveData;
    }
}
