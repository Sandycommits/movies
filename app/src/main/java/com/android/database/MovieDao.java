package com.android.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.android.model.Movie;

import java.util.List;

@Dao
public interface MovieDao {

    @Query("SELECT * FROM movie")
    List<Movie> getAllLikedMovies();

    @Insert
    void insert(Movie movie);

    @Delete
    void remove(Movie movie);
}
