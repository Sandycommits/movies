package com.android.movies;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.android.model.MovieDetails;
import com.android.movies.databinding.ActivityMovieDetailsBinding;
import com.android.utils.ProcessingState;
import com.android.utils.State;
import com.android.utils.ViewModelFactory;
import com.android.viewmodel.MoviesViewModel;
import com.squareup.picasso.Picasso;

public class MovieDetailsActivity extends BaseActivity {

    private static final String TAG = MovieDetailsActivity.class.getSimpleName();
    MoviesViewModel moviesViewModel;
    private ActivityMovieDetailsBinding movieDetailsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movieDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);
        moviesViewModel = new ViewModelProvider(this, new ViewModelFactory(this.getApplication())).get(MoviesViewModel.class);
        String name = getIntent().getStringExtra("name");
        if (isNetworkConnected()) {
            showProgressDialog();
            moviesViewModel.fetchMovieDetails(name).observe(this, this::onMovieDetailsFetch);
        } else {
            Toast.makeText(this, "No Network", Toast.LENGTH_LONG).show();
        }
    }

    private void onMovieDetailsFetch(ProcessingState state) {
        hideProgressDialog();
        if (state.currentState() == State.SUCCESS) {
            MovieDetails movieDetails = (MovieDetails) state.extras();
            movieDetailsBinding.setMovieDetails(movieDetails);
            Picasso.get().load(movieDetails.getPoster()).into(movieDetailsBinding.poster);
        } else {
            Log.e(TAG, "Something went wrong");
        }
    }
}
