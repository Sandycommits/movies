package com.android.movies;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.database.AppDatabase;
import com.android.model.Movie;
import com.android.model.MovieSearchResponse;
import com.android.movies.databinding.ActivityMainBinding;
import com.android.utils.LikedMoviesAdapter;
import com.android.utils.MoviesAdapter;
import com.android.utils.OnLikeClickedListener;
import com.android.utils.PaginationListener;
import com.android.utils.ProcessingState;
import com.android.utils.State;
import com.android.utils.ViewModelFactory;
import com.android.viewmodel.MoviesViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.android.utils.PaginationListener.PAGE_START;

public class MainActivity extends BaseActivity implements SearchView.OnQueryTextListener, SwipeRefreshLayout.OnRefreshListener, OnLikeClickedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    ActivityMainBinding activityMainBinding;
    MoviesViewModel moviesViewModel;
    MoviesAdapter adapter;
    LikedMoviesAdapter likedMoviesAdapter;
    String query = "Batman";
    @Inject
    AppDatabase appDatabase;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private List<Movie> likedMovies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        moviesViewModel = new ViewModelProvider(this, new ViewModelFactory(this.getApplication())).get(MoviesViewModel.class);
        MyApplication myApplication = (MyApplication) getApplication();
        myApplication.getAppComponent().inject(this);
        showProgressDialog();
        moviesViewModel.fetchLikedMoviesList().observe(this, this::getLikedMovies);
        initializeRecycler();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint(getString(R.string.search_movie));
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        this.query = query;
        currentPage = PAGE_START;
        isLastPage = false;
        initializeRecycler();
        fetchMovies(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onRefresh() {
        currentPage = PAGE_START;
        isLastPage = false;
        adapter.clear();
        fetchMovies(query);
    }

    @Override
    public void onLike(Movie movie) {
        showProgressDialog();
        moviesViewModel.saveMovie(movie).observe(this, this::onInsertOrDelete);
    }

    @Override
    public void onUnLike(Movie movie) {
        showProgressDialog();
        moviesViewModel.removeMovie(movie).observe(this, this::onInsertOrDelete);
    }

    private void initializeRecycler() {
        activityMainBinding.swipeRefresh.setOnRefreshListener(this);
        activityMainBinding.moviesRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        activityMainBinding.moviesRecyclerView.setLayoutManager(layoutManager);
        adapter = new MoviesAdapter(new ArrayList<>(), this);
        activityMainBinding.moviesRecyclerView.setAdapter(adapter);
        // add scroll listener while user reach in bottom load more will call
        activityMainBinding.moviesRecyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                fetchMovies(query);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void onInsertOrDelete(ProcessingState state) {
        hideProgressDialog();
        if (state.currentState() == State.SUCCESS) {
            Log.i(TAG, "Movie inserted or removed");
        } else {
            Log.e(TAG, getString(R.string.error_message));
        }
    }

    private void getLikedMovies(ProcessingState state) {
        hideProgressDialog();
        if (state.currentState() == State.SUCCESS) {
            likedMovies = (List<Movie>) state.extras();
            displayLikedMovies();
            fetchMovies(query);
        } else {
            Log.e(TAG, getString(R.string.error_message));
        }
    }

    private void displayLikedMovies() {
        if (!likedMovies.isEmpty()) {
            activityMainBinding.likedLabel.setVisibility(View.VISIBLE);
            activityMainBinding.likedMovieRecyclerView.setVisibility(View.VISIBLE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
            activityMainBinding.likedMovieRecyclerView.setLayoutManager(layoutManager);
            likedMoviesAdapter = new LikedMoviesAdapter(likedMovies, this);
            activityMainBinding.likedMovieRecyclerView.setAdapter(likedMoviesAdapter);
        }
    }

    private void onFetchMoviesList(ProcessingState state) {
        hideProgressDialog();
        if (state.currentState() == State.SUCCESS) {
            MovieSearchResponse movieSearchResponse = (MovieSearchResponse) state.extras();
            int totalMovies = movieSearchResponse.getTotalResults();
            int totalPage = totalMovies / 10;
            if (currentPage != PAGE_START) adapter.removeLoading();
            List<Movie> movies = movieSearchResponse.getMoviesList();
            checkLikedMovies(movies);
            adapter.addMovies(movies);
            activityMainBinding.swipeRefresh.setRefreshing(false);
            // check weather is last page or not
            if (currentPage < totalPage) {
                adapter.addLoading();
            } else {
                isLastPage = true;
            }
            isLoading = false;
        } else {
            Log.e(TAG, getString(R.string.error_message));
        }
    }

    private void checkLikedMovies(List<Movie> movies) {
        for (Movie movie : movies) {
            checkMovieIfLiked(movie);
        }
    }

    private void checkMovieIfLiked(Movie movie) {
        for (Movie likedMovie : likedMovies) {
            if (likedMovie.getId().equalsIgnoreCase(movie.getId())) {
                movie.setChecked(true);
            }
        }
    }

    private void fetchMovies(String query) {
        if (isNetworkConnected()) {
            showProgressDialog();
            moviesViewModel.fetchMoviesList(query, currentPage).observe(this, this::onFetchMoviesList);
        } else {
            Toast.makeText(this, "No Network", Toast.LENGTH_LONG).show();
        }
    }
}
