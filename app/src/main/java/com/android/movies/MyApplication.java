package com.android.movies;

import android.app.Application;

import com.android.dagger.AppComponent;
import com.android.dagger.AppModule;
import com.android.dagger.DaggerAppComponent;
import com.android.utils.ProgressDialogUtils;

import lombok.Getter;
import lombok.Setter;

public class MyApplication extends Application {

    @Getter
    protected AppComponent appComponent;
    @Setter
    @Getter
    private ProgressDialogUtils progressDialog;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this)).build();
    }
}
