package com.android.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.movies.MyApplication;
import com.android.movies.R;

/**
 * Util Class for displaying and dismissing Custom ProgressDialog
 */

public class ProgressDialogUtils extends Dialog {


    private ProgressDialogUtils(Context context) {
        super(context, R.style.progressDialogTheme);
    }

    public static ProgressDialogUtils show(final Activity context) {
        return show(context, null, null);
    }

    private static ProgressDialogUtils show(Activity context, CharSequence title,
                                            CharSequence message) {
        return show(context, title, message, false);
    }

    private static ProgressDialogUtils show(Activity context, CharSequence title,
                                            CharSequence message, boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }


    private static ProgressDialogUtils show(Activity context, CharSequence title,
                                            CharSequence message, boolean indeterminate,
                                            boolean cancelable, OnCancelListener cancelListener) {


        MyApplication myApplication = (MyApplication) context.getApplication();

        ProgressDialogUtils existingDialog = myApplication.getProgressDialog();
        if (existingDialog != null && existingDialog.isShowing()) {
            if (!context.isFinishing() && !context.isDestroyed()) {
                existingDialog.dismiss();
            }
        }
        ProgressDialogUtils progressDialog = new ProgressDialogUtils(context);
        progressDialog.setCancelable(false);
        progressDialog.setOnCancelListener(cancelListener);

        /* The next line will add the ProgressBar to the progressDialog. */
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        ProgressBar progressBar = new ProgressBar(context);
        progressDialog.addContentView(progressBar, layoutParams);


        if (!context.isFinishing() && !context.isDestroyed()) {
            progressDialog.show();
            myApplication.setProgressDialog(progressDialog);
        }


        return progressDialog;
    }


    /*Dismisses the progressDialog*/
    public static void dismissDialog(Activity activity) {
        MyApplication myApplication = (MyApplication) activity.getApplication();
        ProgressDialogUtils progressDialog = myApplication.getProgressDialog();
        if (activity != null) {
            if (!activity.isFinishing() && !activity.isDestroyed()) {
                if (progressDialog != null) {
                    progressDialog.setCancelable(true);
                    progressDialog.dismiss();
                }
            }
        }
    }


}