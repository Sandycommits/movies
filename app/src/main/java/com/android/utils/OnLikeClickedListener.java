package com.android.utils;

import com.android.model.Movie;

public interface OnLikeClickedListener {
    void onLike(Movie movie);

    void onUnLike(Movie movie);
}
