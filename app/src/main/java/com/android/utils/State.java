package com.android.utils;

public enum State {
    LOADING(0), SUCCESS(1), FAILED(-1);

    private int value;

    State(int val) {
        value = val;
    }

    public int getValue() {
        return value;
    }
}
