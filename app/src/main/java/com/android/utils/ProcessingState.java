package com.android.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@AllArgsConstructor
@Data
@Accessors(fluent = true)
public class ProcessingState {
    public static final ProcessingState errorState = new ProcessingState(State.FAILED, new Throwable(), null);
    public static final ProcessingState loadingState = new ProcessingState(State.LOADING, null, new Object());
    public static final ProcessingState successState = new ProcessingState(State.SUCCESS, null, new Object());
    State currentState;
    Throwable error;
    private Object extras;
}
