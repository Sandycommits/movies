package com.android.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.model.Movie;
import com.android.movies.MovieDetailsActivity;
import com.android.movies.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LikedMoviesAdapter extends RecyclerView.Adapter<LikedMoviesAdapter.CardHolder> {

    private List<Movie> likedMovies;

    private OnLikeClickedListener listener;
    private Context context;

    // Pass in the contact array into the constructor
    public LikedMoviesAdapter(List<Movie> movies, Context context) {
        this.likedMovies = movies;
        this.listener = (OnLikeClickedListener) context;
        this.context = context;
    }

    @NonNull
    @Override
    public CardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CardHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.liked_movie_card_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CardHolder holder, int position) {
        Movie likedMovie = likedMovies.get(position);
        holder.title.setText(likedMovie.getTitle());
        Picasso.get().load(likedMovie.getPoster()).into(holder.poster);
        holder.likeButton.setOnCheckedChangeListener(null);
        holder.likeButton.setChecked(likedMovie.isChecked());
        holder.likeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    listener.onLike(likedMovie);
                } else {
                    listener.onUnLike(likedMovie);
                }
                likedMovie.setChecked(isChecked);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetailsActivity.class);
                intent.putExtra("name", likedMovie.getTitle());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return likedMovies == null ? 0 : likedMovies.size();
    }


    class CardHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView poster;
        CheckBox likeButton;

        CardHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.movieName);
            poster = itemView.findViewById(R.id.poster);
            likeButton = itemView.findViewById(R.id.likeButton);
        }
    }

}
