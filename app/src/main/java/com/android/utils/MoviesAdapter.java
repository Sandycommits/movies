package com.android.utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.model.Movie;
import com.android.movies.MovieDetailsActivity;
import com.android.movies.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_LOADING = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;

    private List<Movie> movies;

    private OnLikeClickedListener listener;
    private Context context;

    // Pass in the contact array into the constructor
    public MoviesAdapter(List<Movie> movies, Context context) {
        this.movies = movies;
        this.listener = (OnLikeClickedListener) context;
        this.context = context;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new CardHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_card_item, parent, false));
            case VIEW_TYPE_LOADING:
                return new ProgressHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == movies.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }
    }

    @Override
    public int getItemCount() {
        return movies == null ? 0 : movies.size();
    }

    public void addMovies(List<Movie> moreMovies) {
        movies.addAll(moreMovies);
        notifyDataSetChanged();
    }

    public void addLoading() {
        isLoaderVisible = true;
        movies.add(new Movie());
        notifyItemInserted(movies.size() - 1);
    }

    public void removeLoading() {
        isLoaderVisible = false;
        int position = movies.size() - 1;
        Movie item = getMovie(position);
        if (item != null) {
            movies.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        movies.clear();
        notifyDataSetChanged();
    }

    private Movie getMovie(int position) {
        return movies.get(position);
    }

    class CardHolder extends BaseViewHolder {
        TextView title;
        TextView year;
        ImageView poster;
        CheckBox likeButton;

        CardHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.movieName);
            year = itemView.findViewById(R.id.releaseDate);
            poster = itemView.findViewById(R.id.poster);
            likeButton = itemView.findViewById(R.id.likeButton);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            Movie movie = movies.get(position);
            title.setText(movie.getTitle());
            year.setText(movie.getYear());
            Picasso.get().load(movie.getPoster()).into(poster);
            likeButton.setOnCheckedChangeListener(null);
            likeButton.setChecked(movie.isChecked());
            likeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        listener.onLike(movie);
                    } else {
                        listener.onUnLike(movie);
                    }
                    movie.setChecked(isChecked);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MovieDetailsActivity.class);
                    intent.putExtra("name", movie.getTitle());
                    context.startActivity(intent);
                }
            });
        }
    }

    public class ProgressHolder extends BaseViewHolder {

        ProgressBar progressBar;

        ProgressHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }

        @Override
        protected void clear() {
        }
    }
}
